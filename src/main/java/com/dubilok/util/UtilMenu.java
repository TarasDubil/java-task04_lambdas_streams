package com.dubilok.util;

import com.dubilok.model.ex_1.Functional;
import com.dubilok.view.Printable;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Map;

public class UtilMenu {

    private static Logger logger = LogManager.getLogger(UtilMenu.class);

    private static void outputMenu(Map<String, String> menu) {
        System.out.println("\nMENU:");
        menu.values().forEach(System.out::println);
    }

    public static void show(BufferedReader bufferedReader, Map<String, String> menu, Map<String, Printable> methodsMenu) {
        String keyMenu = null;
        do {
            outputMenu(menu);
            System.out.println("Please, select menu point.");
            try {
                keyMenu = bufferedReader.readLine().toUpperCase();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }

    public static void readThirdIntegerValues(BufferedReader bufferedReader, Functional functional) throws IOException {
        Integer first, second, third;
        logger.info("Enter your first Integer:");
        first = Integer.valueOf(bufferedReader.readLine());
        logger.info("Enter your second Integer:");
        second = Integer.valueOf(bufferedReader.readLine());
        logger.info("Enter your third Integer:");
        third = Integer.valueOf(bufferedReader.readLine());
        Object obj = functional.accept(first, second, third);
        logger.info("Result = " + obj);
    }

}
