package com.dubilok.service;

import com.dubilok.model.ex_3.GetList;
import com.dubilok.model.ex_3.Methods;
import com.dubilok.util.UtilMenu;
import com.dubilok.view.Printable;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class ManagerThirdEx implements Manager {

    private Map<String, String> menu;
    private List<Integer> list = GetList.getListByGenerate();
    private Map<String, Printable> methodsMenu;
    private BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

    public ManagerThirdEx() {
        menu = new LinkedHashMap<>();
        menu.put("1", "  1 - get list by generate");
        menu.put("2", "  2 - get list by iterate");
        menu.put("3", "  3 - get list by builder");
        menu.put("4", "  4 - get min value in list");
        menu.put("5", "  5 - get max value in list");
        menu.put("6", "  6 - get sum elements by sum Stream");
        menu.put("7", "  7 - get sum elements by reduce");
        menu.put("8", "  8 - get average value in list");
        menu.put("9", "  9 - get count value bigger than average in list");
        menu.put("10", "  10 - print list");
        menu.put("Q", "  Q - exit");
        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
        methodsMenu.put("3", this::pressButton3);
        methodsMenu.put("4", this::pressButton4);
        methodsMenu.put("5", this::pressButton5);
        methodsMenu.put("6", this::pressButton6);
        methodsMenu.put("7", this::pressButton7);
        methodsMenu.put("8", this::pressButton8);
        methodsMenu.put("9", this::pressButton9);
        methodsMenu.put("10", this::pressButton10);
    }

    private void pressButton1() {
        list = GetList.getListByGenerate();
    }

    private void pressButton2() {
        list = GetList.getListByIterate();
    }

    private void pressButton3() {
        list = GetList.getListByBuilder();
    }

    private void pressButton4() {
        Methods.getMinValueByList(list);
    }

    private void pressButton5() {
        Methods.getMaxValueByLIst(list);
    }

    private void pressButton6() {
        Methods.getSumAllElementBySumStream(list);
    }

    private void pressButton7() {
        Methods.getSumAllElementByReduce(list);
    }

    private void pressButton8() {
        Methods.getAverageValueByList(list);
    }

    private void pressButton9() {
        Methods.getCountValueBiggerThanAverage(list);
    }

    private void pressButton10() {
        list.forEach(System.out::println);
    }

    @Override
    public void showMethods() {
        UtilMenu.show(bufferedReader, menu, methodsMenu);
    }
}
