package com.dubilok.service;

import com.dubilok.model.ex_3.GetList;
import com.dubilok.model.ex_4.Methods;
import com.dubilok.util.UtilMenu;
import com.dubilok.view.Printable;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

public class ManagerFourthEx implements Manager {

    private static Logger logger = LogManager.getLogger(ManagerFourthEx.class);

    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private List<String> list = new ArrayList<>();
    private static BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

    public ManagerFourthEx() {
        menu = new LinkedHashMap<>();
        list.add("Your list!!!!");
        menu.put("1", "  1 - get number of unique words");
        menu.put("2", "  2 - sort list unique words");
        menu.put("3", "  3 - get count symbol in text");
        menu.put("4", "  4 - get count symbol in text except UpperCase");
        menu.put("5", "  5 - print list");
        menu.put("Q", "  Q - exit");
        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
        methodsMenu.put("3", this::pressButton3);
        methodsMenu.put("4", this::pressButton4);
        methodsMenu.put("5", this::pressButton5);
    }

    private void pressButton1() {
        logger.info("Enter your String");
        try {
            Methods.getNumberOfUniqueWords(bufferedReader.readLine());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void pressButton2() {
        logger.info("Enter your String");
        try {
            list = Methods.getUniqueWordsList(bufferedReader.readLine()).get();
            Optional<List<String>> strings = Methods.sortListOfUniqueWords(list);
            strings.get().forEach(k -> logger.info(k));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void pressButton3() {
        try {
            logger.info("Enter your String");
            Map<Object, Long> map = Methods.getWordCountInText(bufferedReader.readLine()).get();
            Methods.printMap(map);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void pressButton4() {
        try {
            logger.info("Enter your String");
            Map<Object, Long> map = Methods.getOccurrenceNumberOfEachSymbolExceptUpperCase(bufferedReader.readLine()).get();
            Methods.printMap(map);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void pressButton5() {
        GetList.printList(list);
    }

    @Override
    public void showMethods() {
        UtilMenu.show(bufferedReader, menu, methodsMenu);
    }
}
