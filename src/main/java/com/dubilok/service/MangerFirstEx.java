package com.dubilok.service;

import com.dubilok.model.ex_1.Functional;
import com.dubilok.util.UtilMenu;
import com.dubilok.view.Printable;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Stream;

public class MangerFirstEx implements Manager {

    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

    public MangerFirstEx() {
        menu = new LinkedHashMap<>();
        menu.put("1", "  1 - use average method");
        menu.put("2", "  2 - use max Method");
        menu.put("Q", "  Q - exit");
        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
    }

    private void pressButton1() {
        try {
            Functional average = (a, b, c) -> (a + b + c) / 3;
            UtilMenu.readThirdIntegerValues(bufferedReader, average);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void pressButton2() {
        try {
            Functional maxValue = (a, b, c) -> Stream.of(a,b,c).max(Integer::compareTo).get();
            UtilMenu.readThirdIntegerValues(bufferedReader, maxValue);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void showMethods() {
        UtilMenu.show(bufferedReader, menu, methodsMenu);
    }
}
