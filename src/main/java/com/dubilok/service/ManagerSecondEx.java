package com.dubilok.service;

import com.dubilok.model.ex_2.ManagerCommands;
import com.dubilok.util.UtilMenu;
import com.dubilok.view.Printable;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedHashMap;
import java.util.Map;

public class ManagerSecondEx implements Manager {

    private static Logger logger = LogManager.getLogger(ManagerSecondEx.class);
    private Map<String, String> menu;
    private ManagerCommands managerCommands;
    private Map<String, Printable> methodsMenu;
    private static BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));

    public ManagerSecondEx() {
        menu = new LinkedHashMap<>();
        managerCommands = new ManagerCommands();
        managerCommands.startApp(managerCommands);
        menu.put("1", "  1 - ge by lambda");
        menu.put("2", "  2 - get by Anonymous class");
        menu.put("3", "  3 - get by Command as Object class");
        menu.put("4", "  4 - get by method references");
        menu.put("Q", "  Q - exit");
        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
        methodsMenu.put("3", this::pressButton3);
        methodsMenu.put("4", this::pressButton4);
    }

    private void pressButton1() {
        try {
            logger.info("Enter your String");
            managerCommands.getList().get(0).execute(bufferedReader.readLine());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void pressButton2() {
        try {
            logger.info("Enter your String");
            managerCommands.getList().get(1).execute(bufferedReader.readLine());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void pressButton3() {
        try {
            logger.info("Enter your String");
            managerCommands.getList().get(2).execute(bufferedReader.readLine());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void pressButton4() {
        try {
            logger.info("Enter your String");
            managerCommands.getList().get(3).execute(bufferedReader.readLine());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void showMethods() {
        UtilMenu.show(bufferedReader, menu, methodsMenu);
    }
}
