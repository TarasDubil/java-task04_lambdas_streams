package com.dubilok;

import com.dubilok.view.ViewImpl;

public class App {

    public static void main(String[] args) {
        new ViewImpl().show();
    }

}
