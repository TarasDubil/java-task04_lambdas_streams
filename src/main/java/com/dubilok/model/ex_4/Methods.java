package com.dubilok.model.ex_4;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public interface Methods {

    Logger logger = LogManager.getLogger(Methods.class);

    static Optional<Long> getNumberOfUniqueWords(String string) {
        List<String> list = getUniqueWordsList(string).get();
        Optional<Long> count = Optional.of(Optional.of(list.stream().count()).get());
        logger.info("Count of unique words = " + count.get());
        return count;
    }

    static Optional<List<String>> getUniqueWordsList(String string) {
        List<String> list = getWordsList(string).get();
        return Optional.of(list.stream().distinct().collect(Collectors.toList()));
    }

    private static Optional<List<String>> getWordsList(String string) {
        List<String> list = Arrays.asList(string).stream()
                .map(w -> w.replaceAll("[^a-zA-Z]", " ").trim())
                .map(w -> w.replaceAll("  ", " "))
                .flatMap(e -> Stream.of(e.split(" ")))
                .collect(Collectors.toList());
        return Optional.of(list);
    }

    static Optional<List<String>> sortListOfUniqueWords(List<String> list) {
        logger.info("sorted of unique words list");
        Optional<List<String>> collect = Optional.of(list.stream().sorted().collect(Collectors.toList()));
        return collect;
    }

    static Optional<Map<Object, Long>> getWordCountInText(String string) {
        List<String> list = getWordsList(string).get();
        Map<Object, Long> map = list.stream().collect(Collectors.groupingBy(e -> e, Collectors.counting()));
        printMap(map);
        return Optional.of(map);
    }

    static Optional<Map<Object, Long>> getOccurrenceNumberOfEachSymbolExceptUpperCase(String string) {
        List<String> list = getWordsList(string).get();
        Map<Object, Long> map = list.stream()
                .filter(e -> !(e.charAt(0) >= 65 && e.charAt(0) <= 90))
                .collect(Collectors.groupingBy(e -> e, Collectors.counting()));
        return Optional.of(map);
    }

    static void printMap(Map<Object, Long> map) {
        map.entrySet().forEach((k) -> logger.info(k.getKey() + " " + k.getValue()));
    }
}
