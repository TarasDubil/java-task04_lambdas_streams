package com.dubilok.model.ex_1;

@FunctionalInterface
public interface Functional {
    int accept(int a, int b, int c);
}
