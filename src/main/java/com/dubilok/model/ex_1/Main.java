package com.dubilok.model.ex_1;

import java.util.stream.Stream;

public class Main {

    public static void main(String[] args) {
        Functional average = (a, b, c) -> (a + b + c) / 3;
        Functional maxValue = (a, b, c) -> Stream.of(a,b,c).max(Integer::compareTo).get();
        System.out.println(average.accept(1, 2, 3));
        System.out.println(maxValue.accept(1, 2, 3));
    }
}
