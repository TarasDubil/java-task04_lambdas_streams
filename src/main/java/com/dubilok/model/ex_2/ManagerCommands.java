package com.dubilok.model.ex_2;

import java.util.ArrayList;
import java.util.List;

public class ManagerCommands {
    private List<Command> list = new ArrayList<>();

    public void addCommand(Command command) {
        list.add(command);
    }

    public List<Command> getList() {
        return list;
    }

    public void startApp(ManagerCommands commands) {
        Command commandLambda = (name) -> System.out.println("Lambda " + name);
        Command commandAnonymous = new Command() {
            @Override
            public void execute(String name) {
                System.out.println("Anonymous " + name);
            }
        };
        commands.addCommand(commandLambda);
        commands.addCommand(commandAnonymous);
        commands.addCommand(new CommandAsObjectOfClass());
        commands.addCommand(MethodReferences::execute);
    }

}
