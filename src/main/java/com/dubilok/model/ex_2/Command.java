package com.dubilok.model.ex_2;

@FunctionalInterface
public interface Command {
    void execute(String name);
}
