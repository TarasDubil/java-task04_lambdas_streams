package com.dubilok.model.ex_2;

public class CommandAsObjectOfClass implements Command {
    @Override
    public void execute(String name) {
        System.out.println("CommandAsObjectOfClass " + name);
    }
}
