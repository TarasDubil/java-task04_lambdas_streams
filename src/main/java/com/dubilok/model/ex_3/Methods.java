package com.dubilok.model.ex_3;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;
import java.util.Optional;
import java.util.OptionalInt;

public interface Methods {

    Logger logger = LogManager.getLogger(GetList.class);

    static OptionalInt getMinValueByList(List<Integer> list) {
        OptionalInt min = list.stream().mapToInt(e -> e).min();
        logger.info("Min value by list = " + min.getAsInt());
        return min;
    }

    static OptionalInt getMaxValueByLIst(List<Integer> list) {
        OptionalInt max = list.stream().mapToInt(e -> e).max();
        logger.info("Max value by list = " + max.getAsInt());
        return max;
    }

    static Optional<Integer> getSumAllElementBySumStream(List<Integer> list) {
        Optional<Integer> sum = Optional.of(list.stream().mapToInt(Integer::valueOf).sum());
        logger.info("Sum elements in list by stream = " + sum.get());
        return sum;
    }

    static Optional<Integer> getSumAllElementByReduce(List<Integer> list) {
        Optional<Integer> sum = Optional.of(list.stream().reduce((first, second) -> first + second).get());
        logger.info("Sum elements in list by stream = " + sum.get());
        return sum;
    }

    static Optional<Double> getAverageValueByList(List<Integer> list) {
        Optional<Double> average = Optional.of(list.stream().mapToInt(Integer::valueOf).average().getAsDouble());
        logger.info("Average value in list = " + average.get());
        return average;
    }

    static Optional<Long> getCountValueBiggerThanAverage(List<Integer> list) {
        double average = getAverageValueByList(list).get();
        Optional<Long> count = Optional.of(list.stream().mapToDouble(Double::valueOf)
                .filter((e) -> e > average).count());
        logger.info("Count elements bigger than average = " + count.get());
        return count;
    }
}
