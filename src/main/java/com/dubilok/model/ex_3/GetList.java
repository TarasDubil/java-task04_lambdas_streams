package com.dubilok.model.ex_3;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public interface GetList {

    Logger logger = LogManager.getLogger(GetList.class);

    static List<Integer> getListByIterate() {
        List<Integer> list = Stream.iterate(40, n -> n + 2).limit(20).collect(Collectors.toList());
        return list;
    }

    static List<Integer> getListByGenerate() {
        return Stream.generate(() -> 1 + 10).limit(10).collect(Collectors.toList());
    }

    static List<Integer> getListByBuilder() {
        return Stream.<Integer>builder().add(1).add(77).add(99)
                .add(123).add(88).add(0).build().collect(Collectors.toList());
    }

    static void printList(List<String> list) {
        list.forEach(e -> {
            logger.info("element: " + e);
        });
    }
}