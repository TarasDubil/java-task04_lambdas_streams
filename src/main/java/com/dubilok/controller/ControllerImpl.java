package com.dubilok.controller;

import com.dubilok.service.*;

public class ControllerImpl implements Controller {

    private Manager managerFirst;
    private Manager managerSecond;
    private Manager managerThird;
    private Manager managerFourth;

    public ControllerImpl() {
        managerFirst = new MangerFirstEx();
        managerSecond = new ManagerSecondEx();
        managerThird = new ManagerThirdEx();
        managerFourth = new ManagerFourthEx();
    }

    @Override
    public void showFirstExMethods() {
        managerFirst.showMethods();
    }

    @Override
    public void showSecondExMethods() {
        managerSecond.showMethods();
    }

    @Override
    public void showThirdExMethods() {
        managerThird.showMethods();
    }

    @Override
    public void showFourthExMethods() {
        managerFourth.showMethods();
    }
}
