package com.dubilok.controller;

public interface Controller {
    void showFirstExMethods();

    void showSecondExMethods();

    void showThirdExMethods();

    void showFourthExMethods();
}
